# Emacs šalabahter

## Pokretanje, otvaranje datoteka i izlaz

- `$ emacs -nw file.txt` - pokreni u terminalu i otvori `file.txt`
- `$ emacs -nw +i file.txt` - otvori `file.txt` i skoči na redak `i` (`+i:j` za redak `i` i znak/stupac `j`)
- `Ctrl`+`x`, `Ctrl`+`f` - pronađi datoteku za otvaranje
- `Ctrl`+`x`, `Ctrl`+`c` - izađi

## Spremanje

- `Ctrl`+`x`, `Ctrl`+`s` - spremi (trenutni prozor)
- `Ctrl`+`x`, `s` - spremi (svi prozori)

## Uređivanje teksta

- `Ctrl`+`d` - obriši znak ispred kursora (isto kao `Delete`)
- `Meta`+`d` - obriši riječ ispred kursora
- `Meta`+`Backspace` - obriši riječ iza kursora
- `Ctrl`+`_` ili `Ctrl`+`x`, `u` - korak unazad (*undo*)
- `Ctrl`+`Space` - označi početak *kill ring*a (*clipboard*)
- `Meta`+`w` - kopiraj označeno
- `Ctrl`+`w` - izreži označeno
- `Ctrl`+`k` - izreži od kursora do kraja linije
- `Meta`+`k` - izreži od kursora do kraja rečenice
- `Ctrl`+`y` - zalijepi
- `Meta`+`y` - nakon naredbe zalijepi ova naredba ciklira stariji sadržaj *kill ring*a
- `Meta` + `%` - nađi i zamijeni (`y` potvrdi, `n` preskoči, `!` zamijeni sve)

## Kretanje

- `Ctrl`+`l` - centriraj prozor u odnosu na kursor
- `Ctrl`+`f` - 1 znak naprijed (isto kao `→`)
- `Ctrl`+`b` - 1 znak nazad (isto kao `←`)
- `Meta`+`f` - preskoči riječ desno
- `Meta`+`b` - preskoči riječ lijevo
- `Ctrl`+`n` - sljedeći redak (isto kao `↓`)
- `Ctrl`+`p` - prethodni redak (isto kao `↑`)
- `Meta`+`e` - preskoči rečenicu desno
- `Meta`+`a` - preskoči rečenicu lijevo
- `Ctrl`+`e` - kraj retka
- `Ctrl`+`a` - početak retka
- `Ctrl`+`v` ili `PgDn` - skrolaj dolje za jedan prozor
- `Meta`+`v` ili `PgUp` - skrolaj gore za jedan prozor
- `Meta`+`g`, `g` - idi na određeni redak
- `Ctrl`+`s` - traži tekst, od kursora naprijed (opet ista naredba za sljedeću instancu)
- `Ctrl`+`r` - traži tekst, od kursora nazad (opet ista naredba za sljedeću instancu)

## Upravljanje prozorima

- `Ctrl`+`x`, `2` - podijeli na 2 prozora (gore/dolje)
- `Ctrl`+`x`, `3` - podijeli na 2 prozora (lijevo/desno)
- `Ctrl`+`x`, `o` - prijeđi na sljedeći prozor
- `Ctrl`+`x`, `0` - zatvori trenutni prozor
- `Ctrl`+`x`, `1` - zatvori sve prozore osim trenutnog
- `Ctrl`+`x`, `Ctrl`+`b` - izlistaj buffere
- `Ctrl`+`Meta`+`v` - skrolaj neaktivni (sljedeći) prozor

## Razno

- `Ctrl`+`g` ili `Esc`, `Esc`, `Esc` - prekini pisanje naredbe
- `Ctrl`+`h`, `c`, `<NAREDBA>` - ispiši kratak opis za `<NAREDBA>`
- `Ctrl`+`h`, `k`, `<NAREDBA>` - otvori dokumentaciju za `<NAREDBA>`
- `Ctrl`+`u`, `<BROJ>`, `<NAREDBA>` - `<BROJ>` će biti prefiks za `<NAREDBA>` (npr. izvedi `Ctrl`+`n` ukupno `<BROJ>` puta)
- `Ctrl` + `x`, `Ctrl` + `+` - povećaj font za trenutnu sesiju
- `Ctrl` + `x`, `Ctrl` + `-` - smanji font za trenutnu sesiju
- `Ctrl` + `x`, `Ctrl` + `0` - zadana veličina fonta za trenutnu sesiju

## Nota bene

- `Meta` može biti `Alt`, `Esc`, Windows tipka itd.
